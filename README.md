App Engine Java SpringBoot Kotlin application
===

## Sample SpringBoot application written in Kotlin for use with App Engine Java8 Standard.

Based on this [example](https://github.com/GoogleCloudPlatform/getting-started-java/tree/master/appengine-standard-java8/kotlin-sb-appengine-standard) from Google but with Gradle instead of Maven.

Change to your project id in the `build.gradle.kts-file`
## Gradle
### Running locally

`gradle appengineRun`

Make sure you use Java 8 or a NullPointerException will probably occur. 

To use visit: http://localhost:8080/

### Deploying

`gradle appengineDeploy`

To use visit:  Whatever URL your project gets
