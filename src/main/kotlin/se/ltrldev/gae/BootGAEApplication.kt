package se.ltrldev.gae

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@SpringBootApplication
class BootGAEApplication : SpringBootServletInitializer() {
    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(BootGAEApplication::class.java)
    }
}

fun main(args: Array<String>) {
    runApplication<BootGAEApplication>(*args)
}

@RestController
@RequestMapping("/")
class Controller() {

    @GetMapping(produces = ["application/json"])
    fun index(): String {
        return "Up and running..."
    }

    @GetMapping("greetings/{name}", produces = ["application/json"])
    fun greeting(@PathVariable name: String): String {
        return "Hello $name"
    }

}

